CREATE TABLE product (
    id VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    category VARCHAR NOT NULL,
    price DECIMAL NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (name)
);


