package id.pmg.testcrud.controller;

import id.pmg.testcrud.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//@RestController @RequestMapping("/api/product")
@Controller
public class HomeController {
    @Autowired private ProductRepository productRepository;

    @RequestMapping("/")
    public String showHomePage(){
        return "index";
    }

}
