package id.pmg.testcrud.controller;

import id.pmg.testcrud.entity.Product;
import id.pmg.testcrud.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//@RestController @RequestMapping("/api/product")
@Controller
@RequestMapping("products")
public class ProductController {
    @Autowired private ProductRepository productRepository;

    @GetMapping(value = "/list")
    public String list(Model model) {
        model.addAttribute("listProducts", productRepository.findAll());
        return "products/list";

    }




    @GetMapping("/create")
    public String add(Model model) {
        model.addAttribute("product", new Product());
        return "products/form";

    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable String id, Model model) {
        model.addAttribute("product", productRepository.findById(id));
        return "products/form";

    }

    @PostMapping(value = "/save")
    public String save(Product product, final RedirectAttributes ra) {
        Product save = productRepository.save(product);
        return "redirect:/products/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(Product id) {
        productRepository.delete(id);
        return "redirect:/products/list";
    }

}
