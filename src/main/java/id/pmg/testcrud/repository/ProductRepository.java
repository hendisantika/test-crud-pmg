package id.pmg.testcrud.repository;

import id.pmg.testcrud.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends PagingAndSortingRepository<Product, String> {
}
